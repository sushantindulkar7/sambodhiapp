import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { EdubiggServiceProvider } from '../../providers/edubigg-service/edubigg-service';
import moment from 'moment';
/**
 * Generated class for the AddLeavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-leave',
  templateUrl: 'add-leave.html',
})
export class AddLeavePage {

  leaveForm: any;
  submitAttempt: any;
  toast : any;
  constructor(private storage: Storage, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public eduService : EdubiggServiceProvider,private toastCtrl: ToastController,public viewCtrl : ViewController) {
    this.leaveForm = formBuilder.group({
      fromDate: ['', Validators.compose([Validators.required])],
      toDate: ['', Validators.compose([Validators.required])],
      note: ['', Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddLeavePage');
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3500,
      position: 'bottom'
    });
    toast.present();
  }

  addLeave(){
    this.storage.get('userData').then((val) => {
    this.submitAttempt = true;
    if (this.leaveForm.valid) { 
     
        var data = {
          StudentID : val.StudentID,
          BranchID :val.BranchID,
          LeaveOpener : val.Parent,
          LeaveStartDate : this.leaveForm.value.fromDate,
          LeaveEndDate : this.leaveForm.value.toDate,
          LeaveReason : this.leaveForm.value.note,
          ActualTime : moment().format("YYYY-MM-DD"),
        };
  
      this.eduService.addLeave(data).then((leaveData) => {
        console.log("user data = " + JSON.stringify(leaveData));

      //  if(leaveData['Code'] == 1){
      //   this.presentToast("Leave was marked successfully");
      //  }
       this.presentToast(leaveData['Message']|| "Sorry, Something went wrong.");

       this.viewCtrl.dismiss();
      }).catch((error) => {
        this.viewCtrl.dismiss();
        console.log("error = ",error)
      });

      
    }});
  }


}
