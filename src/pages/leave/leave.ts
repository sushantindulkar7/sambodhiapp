import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AddLeavePage } from '../add-leave/add-leave';

import { Storage } from '@ionic/storage';
import * as _ from 'underscore';
import { EdubiggServiceProvider } from '../../providers/edubigg-service/edubigg-service';
/**
 * Generated class for the LeavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leave',
  templateUrl: 'leave.html',
})
export class LeavePage {

  leaveData :any;
  nodata = false;
  constructor(private storage: Storage, public navCtrl: NavController, public navParams: NavParams,public eduService: EdubiggServiceProvider) {
  //this.getLeaveData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeavePage');
  }

  refreshLeaves(refresher) {
    refresher.complete();
    this.getLeaveData();
  }

  openLeaveForm(){
    this.navCtrl.push(AddLeavePage);
  }

  ionViewDidEnter(){
    this.getLeaveData();
  }

  getLeaveData() { 
    var data = null;

    this.storage.get('id').then((val) => {
      console.log('Your id is', val);
      data = {
        StudentId: val,
      };

      this.eduService.getLeave(data).then((response) => {
        console.log("user data = " + JSON.stringify(response));
        if (response['Data'].length == 0) {
          this.leaveData = [];
          this.nodata = true;
        } else {
          this.nodata = false;
          this.leaveData = response['Data'];

        }
      }).catch((error) => {
        console.log("error = ", error)
      });

    });

  }


}
