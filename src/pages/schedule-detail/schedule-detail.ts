import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform,ViewController,AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ScheduleDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule-detail',
  templateUrl: 'schedule-detail.html',
})
export class ScheduleDetailPage {
      StandardName: any;
      SubjectName:any;
      TopicName:any;
      LectureTimeEnd:any;
      LectureTimeStart:any;
      LectureDate:any;
      FacultyName:any;

  constructor(public alertCtrl :AlertController, public viewCtrl:ViewController, public platform: Platform,public navCtrl: NavController, public navParams: NavParams, public storage : Storage) {

      this.StandardName = this.navParams.data.StandardName;
      this.SubjectName = this.navParams.data.SubjectName;
      this.TopicName = this.navParams.data.TopicName;
      this.LectureTimeEnd = this.navParams.data.LectureTimeEnd;
      this.LectureTimeStart = this.navParams.data.LectureTimeStart;
      this.LectureDate = this.navParams.data.LectureDate;
      this.FacultyName = this.navParams.data.FacultyName;

      // platform.registerBackButtonAction(() => {
      //   this.storage.get('page').then((val) => {
      //     console.log("back val ScheduleDetailPage =",val)
      //     if(val == "ScheduleDetailPage"){
      //       this.viewCtrl.dismiss();
      //     }else if(val == "SchedulesPage"){
      //       this.logout();
      //     }else{
      //       console.log("inside else");
      //       this.viewCtrl.dismiss();
      //       //this.navCtrl.pop();
      //     }
      //   })
      // });

      // platform.registerBackButtonAction(() => {
      //   this.viewCtrl.dismiss();
      // })

  }

  // logout(){
  //   let prompt = this.alertCtrl.create({
  //     title: 'Logout',
  //     message: "Are you sure you want to logout?",
  //     buttons: [
  //       {
  //         text: 'Yes',
  //         handler: data => {
  //           this.storage.set('id',null);
  //           this.navCtrl.pop();
  //         }
  //       },
  //         {
  //           text: 'No',
  //           role: 'cancel',
  //           handler: () => {
  //             this.storage.set('page', "SchedulesPage");
  //             console.log('Cancel clicked');
  //           }
  //         }
  //     ]
  //   });
  //   prompt.present();
  // }

  ionViewDidLoad() {
    this.storage.set('page', "ScheduleDetailPage");
    console.log('ionViewDidLoad ScheduleDetailPage');
  }
  ionViewDidEnter(){
    this.storage.set('page', "ScheduleDetailPage");
    console.log("ionViewDidEnter schedule")
  }

  stringIsNumber(s) {
    var x = +s; // made cast obvious for demonstration
    return x.toString() === s;
  }

}
