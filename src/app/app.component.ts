import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, ToastController, AlertController,  NavController,IonicApp, MenuController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
// import { LoginPage } from '../pages/login/login';
// import { ProfilePage } from '../pages/profile/profile';
// import { SchedulesPage } from '../pages/schedules/schedules';

import { SchedulesPage } from '../pages/schedules/schedules';
import { PerformancePage } from '../pages/performance/performance';
import { AttendancePage } from '../pages/attendance/attendance';
import { AdmissionPage } from '../pages/admission/admission'; 
import { ProfilePage } from '../pages/profile/profile';
import { DocumentsPage } from '../pages/documents/documents';
import { FeedbackPage } from '../pages/feedback/feedback';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { LeavePage } from '../pages/leave/leave';
import { LoginPage } from '../pages/login/login';

import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: NavController;

  //rootPage:any = LoginPage;
  rootPage :any;
  items :any;
  studentName  :any;
  studentImage  :any;
  
  pageArray = ["SchedulesPage"];
  constructor(public events : Events, public menuCtrl: MenuController,private ionicApp: IonicApp,public fcm : FCM, public alertCtrl: AlertController,private storage: Storage,public toastCtrl: ToastController,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();


    var data = {
      SchedulesPage : SchedulesPage,
      AttendancePage : AttendancePage,
      AdmissionPage : AdmissionPage
    }
         //back button handle
        //Registration of push in Android and Windows Phone
        var lastTimeBackPress = 0;
        var timePeriodToExit  = 2000;

        events.subscribe('setuserData',() => {
          this.setUserData();
        });
        

        
        


        platform.registerBackButtonAction(() => {
            // get current active page
          //  let view = this.nav.getActive();
        console.log("page array",this.pageArray,this.pageArray.length);
        console.log("this.pageArray = ",this.pageArray[1]);

       // var pageName = this.pageArray[(this.pageArray.length-2)];
        var page1 = this.pageArray[(this.pageArray.length-1)];
        var pageName = this.pageArray[(this.pageArray.indexOf(page1)-1)];

        

        console.log("data[pageName] >",data[pageName]);

        console.log("pageName = ",pageName);
        let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();

        let view = this.nav.getActive();
        let currentRootPage = view.component;
        console.log("activePortal",activePortal);
        console.log("view",view);
        console.log("currentRootPage",currentRootPage);

          
        if (this.menuCtrl.isOpen()) {
          console.log("this.menuCtrl",this.menuCtrl);
          this.menuCtrl.close();
        } else if (view.component.name == "HomePage") {
              
                this.storage.get('page').then((val) => {
                  console.log("back val =",val)
                  if(val == "SchedulesPage"){
                    this.storage.set('page', "");
                    this.pageArray = [];
                    this.logout();
                  }else if(val == "LoginPage"){
                    this.storage.set('page', "");
                    
                    let prompt = this.alertCtrl.create({
                      title: 'Exit',
                      message: "Are you sure you want to exit?",
                      buttons: [
                        {
                          text: 'Yes',
                          handler: data => {
                            this.platform.exitApp(); //Exit from app
                          }
                        },
                          {
                            text: 'No',
                            role: 'cancel',
                            handler: () => {
                              console.log('Cancel clicked');
                            }
                          }
                      ]
                    });
                    prompt.present();

                  }else if(val == "ScheduleDetailPage"){
                    console.log("inside ScheduleDetailPage");
                    this.storage.set('page', "");
                    this.pageArray.push("SchedulesPage");
                    this.rootPage = SchedulesPage;
                  //  this.nav.push(SchedulesPage)
                    //this.nav.pop();
                  }
                  // else if(val == "AttendancePage"){
                  //   console.log("inside AttendancePage");
                  //   this.storage.set('page', "");
                  //   //this.rootPage = HomePage;
                  //   this.nav.push(HomePage)
                  //   //this.nav.pop();
                  // }else if(val == "AdmissionPage"){
                  //   console.log("inside AdmissionPage");
                  //   this.storage.set('page', "");
                  //   //this.rootPage = HomePage;
                  //   this.nav.push(HomePage)
                  //   //this.nav.pop();
                  // }
                });
              //  this.logout();
        }else if (view.component.name == "LoginPage") {
            let prompt = this.alertCtrl.create({
              title: 'Exit',
              message: "Are you sure you want to exit?",
              buttons: [
                {
                  text: 'Yes',
                  handler: data => {
                    this.platform.exitApp(); //Exit from app
                  }
                },
                  {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
              ]
            });
            prompt.present();
          } 
          else if (view.component.name == "SchedulesPage") {
            this.storage.set('page', "");
            this.pageArray = [];
            this.logout();
          }else if (view.component.name == "AttendancePage" || view.component.name == "AdmissionPage" || view.component.name == "PerformancePage" || view.component.name == "FeedbackPage" || view.component.name == "LeavePage" || view.component.name == "ChangePasswordPage" || view.component.name == "ProfilePage" || view.component.name == "DocumentsPage") {
            this.storage.set('page', "");
            this.rootPage = HomePage;
          //  this.nav.push(HomePage)
            this.pageArray.push(pageName);
            //this.rootPage = data[pageName];
            
          }
          // else if (view.component.name == "AdmissionPage") {
          //     this.storage.set('page', "");
          //     console.log("data[pageName] ,",data[pageName]);
          //     this.pageArray.push(pageName);
          //     //this.nav.push(data[pageName])
          //    // this.rootPage = data[pageName];
          //     this.rootPage = HomePage;
          // }
          else{
            
            this.storage.set('page', "");
            this.nav.pop();
            //this.nav.push(pageName)
          }

        if (activePortal) { 
          console.log("activePortal",activePortal);
        //  activePortal.dismiss();
        }
        else if (this.menuCtrl.isOpen()) {
          console.log("this.menuCtrl",this.menuCtrl);
        //  this.menuCtrl.close();
        }
        else if (this.nav.canGoBack() || view && view.isOverlay) {
          console.log("this.nav.canGoBack() ",this.nav.canGoBack())
          // console.log("view ",view)
          // console.log("view.isOverlay ",view.isOverlay)
        //  this.nav.pop();
        
        }
        else if(currentRootPage) { // Could any other page that you consider as your main one
          //this.openPage(this.side_menu_navigation_pages[0]);
          console.log("open another page");
         // this.nav.push(HomePage)
        }
        else {
            console.log("insside else==");
        }

            console.log("view.component.name = ",view.component.name);
            // if (view.component.name == "HomePage") {
              
            //     this.storage.get('page').then((val) => {
            //       console.log("back val =",val)
            //       if(val == "SchedulesPage"){
            //         this.storage.set('page', "");
            //         this.logout();
            //       }
            //     });
            //     //this.logout();
            // }else if (view.component.name == "LoginPage") {
            //   let prompt = this.alertCtrl.create({
            //     title: 'Exit',
            //     message: "Are you sure you want to exit?",
            //     buttons: [
            //       {
            //         text: 'Yes',
            //         handler: data => {
            //           this.platform.exitApp(); //Exit from app
            //         }
            //       },
            //         {
            //           text: 'No',
            //           role: 'cancel',
            //           handler: () => {
            //             console.log('Cancel clicked');
            //           }
            //         }
            //     ]
            //   });
            //   prompt.present();
            // } else {
            //     // go to previous page
            //     this.nav.pop({});
            // }
        });

  }

  logout(){
    let prompt = this.alertCtrl.create({
      title: 'Logout',
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: 'Yes',
          handler: data => {
            
            this.storage.set('id',null);
            
          //  this.rootPage = LoginPage;
            this.nav.push(LoginPage)
          //  this.nav.pop();
          }
        },
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              this.storage.set('page', "SchedulesPage");
              console.log('Cancel clicked');
            }
          }
      ]
    });
    prompt.present();
  }

  setUserData(){
console.log("set menu data");
    this.storage.get('userData').then((val) => {
      console.log("val = ",val);
      if(val.Parent == "Student"){
        console.log("inside if app");
        this.items = [{name:"Schedules",icon:"ios-clock-outline"},
        {name:"Performance",icon:"ios-pulse-outline"},
        {name:"Attendance",icon:"ios-people-outline"},
        {name:"Admission",icon:"ios-paper-outline"},
        {name:"Documents",icon:"ios-document-outline"}, 
        // {name:"Feedback",icon:"ios-chatboxes-outline"},
        {name:"Profile",icon:"ios-person-outline"},
        {name:"Change Password",icon:"ios-lock-outline"}, 
        {name:"Logout",icon:"ios-log-out-outline"}];
      }else{
        console.log("inside else app");
        this.items = [{name:"Schedules",icon:"ios-clock-outline"},
        {name:"Performance",icon:"ios-pulse-outline"},
        {name:"Attendance",icon:"ios-people-outline"},
        {name:"Admission",icon:"ios-paper-outline"},
        {name:"Documents",icon:"ios-document-outline"},    
        {name:"Leaves",icon:"ios-calendar-outline"},
        // {name:"Feedback",icon:"ios-chatboxes-outline"},
        {name:"Profile",icon:"ios-person-outline"},
        {name:"Change Password",icon:"ios-lock-outline"},      
        {name:"Logout",icon:"ios-log-out-outline"}];
      } 

      this.studentName = val.Name;
      // this.storage.get('url').then((val) => {
      //   this.studentImage = (val.StudentImage ? val+"Branch/UploadFiles/"+val.StudentImage:"assets/images/default-user.png")
      // });

      this.studentImage = (val.StudentImage ? val.StudentImage :"assets/images/default-user.png")

    });   

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get("id").then(data=>{
        console.log(data);
        if(data == null || data == undefined ){
          this.rootPage = LoginPage;
        }else{
          this.rootPage = HomePage;
        }
      });

      // get fcm token
      if (this.platform.is('cordova')) {
      this.fcm.getToken().then(token=>{
        console.log("token = ",token);
        this.storage.set('token', token);
      })
      
      this.fcm.onNotification().subscribe(data=>{
        if(data.wasTapped){
          console.log("Received in background");
        } else {
          console.log("Received in foreground");
        };
      })
      
      this.fcm.onTokenRefresh().subscribe(token=>{
        console.log("token on refresh = ",token);
        this.storage.set('token', token);
      })
    }
      //end
    });
  }

  sliderClick(event,data){
    console.log("event 123  : "+JSON.stringify(data));
    switch(data.name){
      case "Schedules":
      this.storage.set('page', "SchedulesPage");
      this.rootPage = SchedulesPage;
      this.pageArray.push("SchedulesPage");
      //this.navCtrl.push(SchedulesPage);
      break;
      case "Performance":
    //  this.nav.pop();
      this.rootPage = PerformancePage;
      this.storage.set('page', "PerformancePage");
      this.pageArray.push("PerformancePage");
      //this.nav.push(PerformancePage);
      break;
      case "Attendance":
   //   this.nav.pop();
      this.rootPage = AttendancePage;
      this.storage.set('page', "AttendancePage");
      this.pageArray.push("AttendancePage");
      //this.nav.push(AttendancePage);
      break;
      case "Admission":
    //  this.nav.pop();
      this.rootPage = AdmissionPage;
      this.storage.set('page', "AdmissionPage");
      this.pageArray.push("AdmissionPage");
      //this.nav.push(AdmissionPage);
      break;
      case "Profile":
    //  this.nav.pop();
      this.rootPage = ProfilePage;
      this.storage.set('page', "ProfilePage");
      this.pageArray.push("ProfilePage");
      //this.nav.push(ProfilePage);
      break;
      case "Documents":
    //  this.nav.pop();
      this.rootPage = DocumentsPage;
      this.storage.set('page', "DocumentsPage");
      this.pageArray.push("DocumentsPage");
      //this.nav.push(DocumentsPage);
      break; 
      case "Change Password":
    //  this.nav.pop();
      this.rootPage = ChangePasswordPage;
      this.storage.set('page', "ChangePasswordPage");
      this.pageArray.push("ChangePasswordPage");
      //this.nav.push(ChangePasswordPage);
      break;
      case "Leaves":
    //  this.nav.pop();
      this.rootPage = LeavePage;
      this.storage.set('page', "LeavePage");
      this.pageArray.push("LeavePage");
     // this.nav.push(LeavePage);
      break;
      case "Feedback":
    //  this.nav.pop();
      this.rootPage = FeedbackPage;
      this.storage.set('page', "FeedbackPage");
      this.pageArray.push("FeedbackPage");
      //this.nav.push(FeedbackPage);
      break;
      case "Logout":
      this.logout()
      break;
      
    }
  }


  
}
